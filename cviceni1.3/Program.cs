﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int cislo1 = 9;
            int cislo2 = 13;
            int cislo3 = 1;
            int pocet = 0;

            if (cislo1 < 10)
                pocet++;
            if (cislo2 < 10)
                pocet++;
            if (cislo3 < 10)
                pocet++;

            Console.WriteLine("Pocet cisel mensich nez 10 je {0}", pocet);
            Console.ReadLine();
        }
    }
}
