﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._9
{
    class Program
    {
        static void Main(string[] args)
        {
            int cislo1 = 9;
            int cislo2 = 9;
            int cislo3 = 13;

            if (cislo1 + cislo2 == cislo3)
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} je cislo {3} souctem cisel {4} a {5}.", cislo1, cislo2, cislo3, cislo3, cislo1, cislo2 );
                Console.ReadLine();
            }

            else if (cislo2 + cislo3 == cislo1)
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} je cislo {3} souctem cisel {4} a {5}.", cislo1, cislo2, cislo3, cislo1, cislo3, cislo2);
                Console.ReadLine();

            }
            else if (cislo1 + cislo3 == cislo2)
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} je cislo {3} souctem cisel {4} a {5}.", cislo1, cislo2, cislo3, cislo2, cislo1, cislo3);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} zadne cislo neni souctem jinych cisel.", cislo1, cislo2, cislo3);
                Console.ReadLine();
            }
        }
    }
}
