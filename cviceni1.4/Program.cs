﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int cislo1 = 5;
            int cislo2 = 17;
            int cislo3 = 3425346;
            int max = cislo1;

            if (cislo2 > max)
                max = cislo2;
            if (cislo3 > max)
                max = cislo3;

            Console.WriteLine("Nejvyssi zadane cislo je {0}", max);
            Console.ReadLine();

        }
    }
}
