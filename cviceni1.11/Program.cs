﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._11
{
    class Program
    {
        static void Main(string[] args)
        {
            int rychlost = 250;
            int pokuta = 0;
            int zbytek = 0;

            if (rychlost < 60)
                pokuta = 1000;
            else if (rychlost > 130 && rychlost < 150)
                pokuta = 2000;
            else if (rychlost > 150)
            {
                zbytek = (rychlost - 150) % 5;
                if (zbytek == 0)
                    pokuta = ((rychlost - 150) / 5) * 500 + 2000;
                else
                    pokuta = (((rychlost - 150) / 5) + 1) * 500 + 2000;
            }
            else
                pokuta = 0;

            if (pokuta > 0)
            {
                Console.WriteLine("Rychlost {0} je pokutovana castkou {1}", rychlost, pokuta);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Rychlost {0} neni pokutovana.", rychlost);
                Console.ReadLine();
            }
        }
    }
}
