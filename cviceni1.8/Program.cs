﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._8
{
    class Program
    {
        static void Main(string[] args)
        {
            int c1 = 6;
            int c2 = 3;
            int c3 = 8;
            int del = 0;
            int dlc = 0;
            int flag = 0;

            if (c1 % c2 == 0)
            {
                del = c1;
                dlc = c2;
                flag = 1;
            }

            if (c2 % c3 == 0)
            {
                del = c2;
                dlc = c3;
                flag = 1;
            }

            if (c1 % c3 == 0)
            {
                del = c1;
                dlc = c3;
                flag = 1;
            }

            if (c2 % c1 == 0)
            {
                del = c2;
                dlc = c1;
                flag = 1;
            }

            if (c3 % c1 == 0)
            {
                del = c3;
                dlc = c1;
                flag = 1;
            }

            if (c3 % c2 == 0)
            {
                del = c3;
                dlc = c2;
                flag = 1;
            }

            if (flag == 1)
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} je cislo {3} delielem cisla {4}.", c1, c2, c3, dlc, del);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Ze zadanych cisel {0}, {1}, {2} neni zadne cislo delitelem jineho.", c1, c2, c3);
                Console.ReadLine();
            }
        }
    }
}
