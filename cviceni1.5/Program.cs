﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._5
{
    class Program
    {
        static void Main(string[] args)
        {
            int cislo1 = 15;
            int cislo2 = 20;
            int cislo3 = 30;
            int max = cislo1;
            int min = cislo2;
            int mid = 0;

            if (cislo2 > max)
                max = cislo2;
            if (cislo3 > max)
                max = cislo3;

            if (cislo1 < min)
                min = cislo1;
            if (cislo3 < min)
                min = cislo3;

            mid = cislo1 + cislo2 + cislo3 - min - max;
            Console.WriteLine("Prostredni cislo z cisel {0}, {1}, {2} je {3}", cislo1, cislo2, cislo3, mid);
            Console.ReadLine();
        }
    }
}
