﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._10
{
    class Program
    {
        static void Main(string[] args)
        {
            int vek = 67;
            int jizdne = 24;

            if (vek < 6)
                jizdne = 0;
            else if ((vek >= 6 && vek < 15) || vek > 60)
                jizdne = 12;
            else
                jizdne = 24;
            Console.WriteLine("Vek cestujiciho je {0}, takze jeho jizdne stoji {1} Kc.", vek, jizdne);
            Console.ReadLine();
        }
    }
}
