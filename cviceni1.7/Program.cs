﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._7
{
    class Program
    {
        static void Main(string[] args)
        {
            int c1 = 2;
            int c2 = 3;
            int c3 = 4;
            int suda = 0;
            int licha = 0;

            if (c1 % 2 == 0)
                suda++;
            else
                licha++;

            if (c2 % 2 == 0)
                suda++;
            else
                licha++;

            if (c3 % 2 == 0)
                suda++;
            else
                licha++;

            if (suda > licha)
            {
                Console.WriteLine("Sudych je vice. Sudych je {0}, lichych je {1}.", suda, licha);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Lichych je vice. Sudych je {0}, lichych je {1}.", suda, licha);
                Console.ReadLine();
            }
        }
    }
}
