﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 7;
            int number2 = 3;
            int rest = number1 / number2;
            Console.WriteLine("Zbytek po deleni cisla {0} cislem {1} je {2}", number1, number2, number1 - rest * number2);
            Console.WriteLine("Pri pouziti modula vyjde {0}", number1 % number2);
            Console.ReadLine();
        }
    }
}
